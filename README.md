# Become a Laravel Developer

Everything you need to know/practice, to become a PHP Laravel Developer. Its a personal learning journey. You may get help from this project, you may also contribute! just make PRs.



![](images/d0o3wb0.jpeg)



# Directory Structure

<pre>
.
├── Algorithms
├── DataStructures
├── DevOps
│   ├── cPanel
│   ├── DigitalOceanLinux
│   └── Docker
├── Documents
├── FunctionalProgramming
├── GitAndGithub
├── GraphQL
├── Laravel
├── Linux
├── OOP
├── PHP
├── ProblemSolving
├── ReactJS
├── RESTApi
├── SQL
├── UnitTesting
├── VsCodeVim
├── Vue
│   ├── VueApollo
│   ├── VueRouter
│   ├── Vuetify
│   └── Vuex
└── WebTechnologies
    ├── AjaxAxiosFetch
    ├── Bootstrap
    ├── CSS
    ├── HTML
    ├── JavaScript
    ├── jQuery
    ├── SASS
    ├── Tailwind
    └── Webpack
</pre>

# Important resources

- Laravel documentation: https://laravel.com/docs
- PHP documentation: https://php.net/manual/en
- JavaScript documentation: https://developer.mozilla.org/en-US
- Vue documentation: https://vuejs.org/v2/guide/



# Connect with project maintainer

<p>
<a href="https://twitter.com/sunwarul"><img src="https://img.shields.io/badge/twitter-%231DA1F2.svg?&style=for-the-badge&logo=twitter&logoColor=white" height=23></a>
<a href="https://medium.com/@sunwarul"><img src="https://img.shields.io/badge/medium-%2312100E.svg?&style=for-the-badge&logo=medium&logoColor=white" height=23></a>
<a href="https://dev.to/sunwarul"><img src="https://img.shields.io/badge/DEV.TO-%230A0A0A.svg?&style=for-the-badge&logo=dev-dot-to&logoColor=white" height=23></a>
<a href="https://facebook.com/sunwarul"><img src="https://img.shields.io/badge/Facebook-blue?&style=for-the-badge&logo=facebook&logoColor=white" height=23></a>
<a href="https://www.quora.com/profile/Sunwarul-Islam"><img src="https://img.shields.io/badge/Quora-red?&style=for-the-badge&logo=quora&logoColor=white" height=23></a>

</p>
